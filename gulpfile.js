const { watch, src, dest } 	= require('gulp');
const less 					= require('gulp-less');
const concat 				= require('gulp-concat')
const plumber				= require('gulp-plumber');
const bump 					= require('gulp-bump');

function css(cb)
{
	src('./css/less/index.less')
	.pipe(concat("compiled.css"))
	.pipe(plumber())
	.pipe(less())
	.pipe(dest('./css'))
	.pipe(concat('compiled.css'))
	.on('end', cb)
	;
	cb();

}

function versionbump(cb) {
	src('./version.ini')
	.pipe(bump())
	.pipe(dest('./'));
	cb();
}

exports.css = css;

exports.bump = versionbump;

exports.default = function() {
	watch(['css/less/*.less'], css);
	watch(['css/compiled.css', 'js/main.js'],versionbump);
}