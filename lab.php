<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>HTML Lab</title>
	<link rel="stylesheet" href="css/compiled.css?v=<?php echo $version; ?>">
	<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
</head>
<body>
	<div class="container">

		<h1>Accordion with side image</h1>
		<div class="sideaccordion">
			<div class="item active">
				<h4 class="title">Title 1</h4>
				<div class="content">Lorem ipsum dolor sit amet.</div>
				<div class="image">
					<img src="img/image1.jpg" width="765" height="512" alt="Image">
				</div>
			</div>
			<div class="item">
				<h4 class="title">Title 2</h4>
				<div class="content">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum cupiditate rem mollitia nobis, ipsa fugit soluta. Blanditiis modi atque consequuntur nam, reprehenderit mollitia, consequatur doloremque illum molestias aspernatur, cum aut.</div>
				<div class="image">
					<img src="img/image2.jpg" width="765" height="512" alt="Image">
				</div>
			</div>
			<div class="item">
				<h4 class="title">Title 3</h4>
				<div class="content">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Animi adipisci magni vero asperiores illo, aperiam commodi rerum eos minus. Dolor, veniam dicta.</div>
				<div class="image">
					<img src="img/image3.jpg" alt="Image">
				</div>
			</div>
		</div>
		<footer>
			<p>Assets version: <?php echo $version; ?></p>
		</footer>
	</div>
	<script src="js/main.js?v=<?php echo $version; ?>"></script>
</body>
</html>